# This is my Charciture Creation Tool. It displays randomized attributes about a fictatious
# person. It displays the first middle and last name of someone, along with their age, location
# hair color, eye color and ethnicity 
# importing needed modules
import random
import pickle
# Creating a class so I can access these variable in the GUI
class Attributes():
       def __init__(self):
              # These are the lists that determin some of the attributes. You can tell if
              # Something I made is a list because it will have the list variable in it.
              # age_attributes sets up a dictionary for the age_range_selector to use to
              # select attributes that are age specicific... like balding or being 5 feet tall.
              self.age_attributes = {1:{'age_range':'Baby','age':[0,1],'height':[20,21], 'weight':[6,21], 'hair':['No Hair']},2:{'age_range':'Toddler','age':[2,3],'height':[34,38],'weight':[27,31], 'hair':[0]},3:{'age_range':'Child','age':[4,5],'height':[40,60],'weight':[40,50], 'hair':[0]},4:{'age_range':'Adolecent','age':[6,12],'height':[45,60],'weight':[45,89], 'hair':[0]},5:{'age_range':'Teenager','age':[13,18],'height':[59,70],'weight':[100,147], 'hair':['No Hair']},6:{'age_range':'Young Adult','age':[19,25],'height':[59,80],'weight':[150,197], 'hair':['No Hair','Grey']},7:{'age_range':'Older Adult','age':[26,39],'height':[59,80],'weight':[150,197], 'hair':['No Hair','Grey','White']},8:{'age_range':'Elderly Person','age':[40,70],'height':[59,80],'weight':[150,197], 'hair':['No Hair','Grey','White']}}
              self.hair_default = ['Black', 'Brown', 'Blond', 'Red']
              self.eye_colors_list = ["Dark Hazel","Dark Green-Hazel","Dark Green","Dark Grey","Dark Brown","Dark Blue","Dark Amber","Light Hazel","Light Green-Hazel","Light Green","Light Grey","Light Brown","Light Blue","Light Amber","Hazel","Green-Hazel","Green","Grey","Brown","Blue","Amber"]
              self.ethnicity_list = ["American Indian", "Alaska Native", "Asian", "African American", "Hispanic", "Pacific Islander", "White"]
              
              
              self.age_range_selector = random.randint(1,8)
              
              # Opens up a previously pickled locations file for a large list of tables.
              # and assigns it to location_list variable
              self.locations_file = open('Locations', 'rb')
              self.location_list = pickle.load(self.locations_file)
              self.locations_file.close

              # Opens up a previously pickled list of names for boys
              # And assigns it to boy_names_list
              self.first_names_boys_file = open('boy_first_names.data','rb')
              self.boy_names_list = pickle.load(self.first_names_boys_file)
              self.first_names_boys_file.close

              # Opens up a previously pickled list of names for girls and assignes it to
              # girl_names_list
              self.first_names_girls_file = open('girl_first_names.data','rb')
              self.girl_names_list = pickle.load(self.first_names_girls_file)
              self.first_names_girls_file.close

              # Sets middle names to be a combination of lists from boy names and girl names
              self.middle_names_list = self.girl_names_list + self.boy_names_list

              # Opens upa previously pickled list of top 1000 last names.
              # And assigns it to the last_name_list variable
              self.last_names_file = open('last_names.data','rb')
              self.last_names_list = pickle.load(self.last_names_file)
              self.last_names_file.close

              # Using the indexes of the location table grabs a country and then a state
              # that is in that country and then a cit that is in that state
              self.country = random.randint(0,len(self.location_list)-1)
              self.state = random.randint(0,len(self.location_list[self.country][1])-1)
              self.town = random.randint(0,len(self.location_list[self.country][1][self.state][1])-1)

              # Grabs the name of the country, name and state that was selected previously
              self.country_name = self.location_list[self.country][0]
              self.state_name = self.location_list[self.country][1][self.state][0]
              self.town_name = self.location_list[self.country][1][self.state][1][self.town]

              
              # defines limitations from range of age by indexing the age_attributes
              # dictionary
              self.age_min = self.age_attributes[self.age_range_selector]['age'][0]
              self.age_max = self.age_attributes[self.age_range_selector]['age'][1]
              
              # defines limitations from range of age to hight by indexing the age_attributes
              # dictionary
              self.height_min = self.age_attributes[self.age_range_selector]['height'][0]
              self.height_max = self.age_attributes[self.age_range_selector]['height'][1]

              # defines limitations from range of age to weight by indexing the age_attributes
              # dictionary
              self.weight_min = self.age_attributes[self.age_range_selector]['weight'][0]
              self.weight_max = self.age_attributes[self.age_range_selector]['weight'][1]
              
              # decides gender
              self.gender_val = random.randint(0,1)
              
              # defines gender val and sets hight and weight adapter, because men are 9 percent taller then women
              # and 19% heavier so when I was gathering the data, I decided to just enter the data from males
              # and remove the 9% and the 19% later if the gender was a girl.
              if self.gender_val == 0:
                     self.gender = 'Male'
                     self.height_adapter = 1
                     self.weight_adapter = 1
              else:
                     self.gender = 'Female'
                     self.height_adapter = 0.91
                     self.weight_adapter = 0.81

              # places age range into a textual setting and sets age following limitations
              self.age_range = self.age_attributes[self.age_range_selector]['age_range']
              self.age = random.randint(self.age_min,self.age_max)
              
              # If the gender is male
              if self.gender == 'Male':
                     if self.age_attributes[self.age_range_selector]['hair'][0] != 0:
                            self.hair_choices = self.hair_default + self.age_attributes[self.age_range_selector]['hair']
                            self.first_name = self.boy_names_list[random.randint(0,len(self.boy_names_list)-1)]
                            # then grab the specifications for hair in the 0th index and add it to the list of 
                            # hair options. And chooses a first name      
       
                     else:
                            self.hair_choices = self.hair_default
                            self.first_name = self.boy_names_list[random.randint(0,len(self.boy_names_list)-1)]
                            # If the 0th index has no hair option then just use the default hair choice and 
                            # pick a name from the boys name lists
              else:
                     if len(self.age_attributes[self.age_range_selector]['hair'])>1:
                            self.hair_choices = self.hair_default + self.age_attributes[self.age_range_selector]['hair'][1:]
                            self.first_name = self.girl_names_list[random.randint(0,len(self.girl_names_list)-1)]
                            # If it is a girl then see how many options for hair there is in your age range. If 2 or more
                            # then add those hair options to your list of hair options. But only past 0th index because that
                            # will make the girl have no hair.
                     else:
                            self.hair_choices = self.hair_default
                            self.first_name = self.girl_names_list[random.randint(0,len(self.girl_names_list)-1)]
                            # If it is a girl, and the index is less than 2 then just use the default
                            # hair groups and pick out a name.
              # middle and last names are gender nutral
              self.middle_name = self.middle_names_list[random.randint(0,len(self.middle_names_list)-1)]
              self.last_name = self.last_names_list[random.randint(0,len(self.last_names_list)-1)]
              # Decide what hair you will have
              self.hair = self.hair_choices[random.randint(0,len(self.hair_choices)-1)]
              # Decide height and weight according to limits and taking into consideration hight changes due to gender
              self.height = random.randint(self.height_min,self.height_max)*self.height_adapter
              self.weight = random.randint(self.weight_min, self.weight_max)*self.weight_adapter
              
              # Decide ethnicity and eye_color
              self.ethnicity = self.ethnicity_list[random.randint(0,len(self.ethnicity_list)-1)]
              self.eye_color = self.eye_colors_list[random.randint(0,17)]
       def get_new_state(self,country):
              self.country = country
              self.state = random.randint(0,len(self.location_list[self.country][1])-1)

              # Grabs the name of the country, name and state that was selected previously
              
              self.state_name = self.location_list[self.country][1][self.state][0]
              return self.state_name
       def get_new_town(self,country,state):
              self.country = country
              self.state = state
              self.town = random.randint(0,len(self.location_list[self.country][1][self.state][1])-1)

              # Grabs the name of the country, name and state that was selected previously
              
             
              self.town_name = self.location_list[self.country][1][self.state][1][self.town]
              return self.town_name


              

# End of the Logic
# Beggining of the GUI


import tkinter
import random

class MyGui():
      def __init__(self):
            
            self.main_window = tkinter.Tk()
            self.main_window.title("-Random Character Maker-")
            self.main_window.geometry("450x450+430+480")
            self.attributes = Attributes()
            # Setting current country and state variables for the change state and city functions
            self.v_country = self.attributes.country
            self.v_state = self.attributes.state
            # Setting up top frame
            self.title_label = tkinter.Label(self.main_window,text='Random Character Maker')
            self.title_label.pack()
            # Name Widgets and Frames
            # Setting up a frame for the name values to occupy
            self.name_frame = tkinter.Frame(self.main_window)
            
            # widgets to occupy name frame
            
            self.name_title = tkinter.Label(self.name_frame,text='Name:')
            self.fn_text = tkinter.StringVar()
            self.fn_text.set(self.attributes.first_name)
            self.fn_label = tkinter.Label(self.name_frame,textvariable=self.fn_text)
            self.fn_button = tkinter.Button(self.name_frame,text='First Name',command=self.change_fname)

            self.mn_text = tkinter.StringVar()
            self.mn_text.set(self.attributes.middle_name)
            self.mn_label = tkinter.Label(self.name_frame,textvariable=self.mn_text)
            self.mn_button = tkinter.Button(self.name_frame,text='Middle Name',command=self.change_mname)
            
            self.ln_text = tkinter.StringVar()
            self.ln_text.set(self.attributes.last_name)
            self.ln_label = tkinter.Label(self.name_frame,textvariable=self.ln_text)
            self.ln_button = tkinter.Button(self.name_frame,text='Last Name',command=self.change_lname)
            self.name_button = tkinter.Button(self.name_frame,text='Full Name',command=self.change_name)
                        
            # packing the name widgets and the frame
            self.name_title.pack(side='left')

            self.ln_label.pack(side='left')
            self.mn_label.pack(side='left')
            self.fn_label.pack(side='left')
            self.name_button.pack(anchor='e')
            self.fn_button.pack(side='right')
            self.mn_button.pack(side='right')
            self.ln_button.pack(side='right')
            self.name_frame.pack(fill="x", padx="3", pady="3")

            # Descripter Widgets and Frames
            # Setting up  a frame for ethnicity
            self.eth_frame = tkinter.Frame(self.main_window)
            self.eth_title = tkinter.Label(self.eth_frame,text='Ethnicity:')
            self.eth_var = tkinter.StringVar()
            self.eth_var.set(self.attributes.ethnicity)
            self.eth_label = tkinter.Label(self.eth_frame,textvariable = self.eth_var)
            self.eth_button = tkinter.Button(self.eth_frame,text='Change Ethnicity',command=self.change_ethnicity)
            # Packing up ethnicity
            self.eth_title.pack(side='left')
            self.eth_label.pack(side='left')
            self.eth_button.pack(anchor='e')
            self.eth_frame.pack(fill="x", padx="3", pady="3")
            # Setting up age discripters and names
            self.age_frame = tkinter.Frame(self.main_window)
            self.age_title = tkinter.Label(self.age_frame,text='Age')
            self.age_var = tkinter.StringVar()
            self.age_var.set(self.attributes.age)
            self.age_label = tkinter.Label(self.age_frame,textvariable=self.age_var)
            self.age_button = tkinter.Button(self.age_frame,text='Change Age',command=self.change_age)
            # Packing up the age 
            self.age_title.pack(side='left')
            self.age_label.pack(side='left')
            self.age_frame.pack(fill="x", padx="3", pady="3")
            self.age_button.pack(anchor='e')
            # Setting up age name
            self.age_name_frame = tkinter.Frame(self.main_window)
            self.age_name_title = tkinter.Label(self.age_name_frame,text='Age name:')
            self.age_name_var = tkinter.StringVar()
            self.age_name_var.set(self.attributes.age_range)
            self.age_name_label = tkinter.Label(self.age_name_frame,textvariable=self.age_name_var)
            # Packing up age name
            self.age_name_title.pack(side='left')
            self.age_name_label.pack(side='left')
            self.age_name_frame.pack(fill="x", padx="3", pady="3")
            # Setting up the Gender
            self.gender_frame = tkinter.Frame(self.main_window)
            self.gender_title = tkinter.Label(self.gender_frame,text='Gender:')
            self.gender_var = tkinter.StringVar()
            self.gender_var.set(self.attributes.gender)
            self.gender_label = tkinter.Label(self.gender_frame,textvariable=self.gender_var)
            self.gender_button = tkinter.Button(self.gender_frame,text='Change Gender',command=self.change_gender)
            # Packing Gender
            self.gender_title.pack(side='left')
            self.gender_label.pack(side='left')
            self.gender_button.pack(anchor='e')
            self.gender_frame.pack(fill="x", padx="3", pady="3")
            # Setting up a frame for hair
            self.hair_frame = tkinter.Frame(self.main_window)
            self.hair_title = tkinter.Label(self.hair_frame,text='Hair Color:')
            self.hair_text = tkinter.StringVar()
            self.hair_text.set(self.attributes.hair)
            self.hair_label = tkinter.Label(self.hair_frame,textvariable=self.hair_text)
            self.hair_button = tkinter.Button(self.hair_frame,text='Change Hair',command=self.change_hair)
            # Packing the hair widgets and frame
            self.hair_title.pack(side='left')
            self.hair_label.pack(side='left')
            self.hair_button.pack(anchor='e')
            self.hair_frame.pack(fill="x", padx="3", pady="3")
            # Setting up frame and widgets for eyes
            self.eye_frame = tkinter.Frame(self.main_window)
            self.eye_title = tkinter.Label(self.eye_frame,text='Eye color:')
            self.eye_text = tkinter.StringVar()
            self.eye_text.set(self.attributes.eye_color)
            self.eye_label = tkinter.Label(self.eye_frame,textvariable=self.eye_text)
            self.eye_button = tkinter.Button(self.eye_frame,text='Change Eye Color',command=self.change_eye_color)
            # Packing up eye frames and eye stuff.
            self.eye_title.pack(side='left')
            self.eye_label.pack(side='left')
            self.eye_button.pack(anchor='e')
            self.eye_frame.pack(fill="x", padx="3", pady="3")

            #Setting up country frames and widgets for country
            self.country_frame = tkinter.Frame(self.main_window)
            self.country_title = tkinter.Label(self.country_frame,text='Country:')
            self.country_var = tkinter.StringVar()
            self.country_var.set(self.attributes.country_name)
            self.country_label = tkinter.Label(self.country_frame,textvariable=self.country_var)
            self.country_button = tkinter.Button(self.country_frame,text='Change Country/State/City',command=self.change_country)
            # Packing up country
            self.country_title.pack(side='left')
            self.country_label.pack(side='left')
            self.country_frame.pack(fill="x", padx="3", pady="3")
            self.country_button.pack(anchor='e')
            # Setting up states and widgets for states
            self.state_frame = tkinter.Frame(self.main_window)
            self.state_title = tkinter.Label(self.state_frame,text='State:')
            self.state_var = tkinter.StringVar()
            self.state_var.set(self.attributes.state_name)
            self.state_label = tkinter.Label(self.state_frame,textvariable=self.state_var)
            self.state_button = tkinter.Button(self.state_frame,text='Change State/City',command=self.change_state)
            # Packing up State
            self.state_title.pack(side='left')
            self.state_label.pack(side='left')
            self.state_button.pack(anchor='e')
            self.state_frame.pack(fill="x", padx="3", pady="3")
            # Setting up City
            self.city_frame = tkinter.Frame(self.main_window)
            self.city_title = tkinter.Label(self.city_frame,text='City/Town:')
            self.city_var = tkinter.StringVar()
            self.city_var.set(self.attributes.town_name)
            self.city_label = tkinter.Label(self.city_frame,textvariable=self.city_var)
            self.city_button = tkinter.Button(self.city_frame,text='Change City',command=self.change_city)
            # Packing up City
            self.city_title.pack(side='left')
            self.city_label.pack(side='left')
            self.city_button.pack(anchor='e')
            self.city_frame.pack(fill="x", padx="3", pady="3")

            # Setting up Time and Date Logic and GUI frames and widgets
            self.month_days = {1:['Janurary',31],2:['Feburary',28],3:['March',31],4:['April',30],5:['May',31],6:['June',30],7:['July',31],8:['August',31],9:['September',30],10:['October',31],11:['November',30],12:['December',31],}
            self.month_val = random.randint(1,len(self.month_days)-1)
            self.month_name = tkinter.StringVar()
            self.month_name = self.month_days[self.month_val][0]
            self.day = random.randint(1,self.month_days[self.month_val][1])
            self.year_start_var = tkinter.StringVar()
            self.year_end_var = tkinter.StringVar()
            self.year_start_var.set('1000')
            self.year_end_var.set('2020')
            self.year_error_text = tkinter.StringVar()
            
            
            self.year = random.randint(int(self.year_start_var.get()),int(self.year_end_var.get()))
            if self.year % 4 == 0 and self.year % 100 == 0 and self.year % 400 == 0:
                  self.month_days[2] = ['Feburary',29]
          
            self.date_frame = tkinter.Frame(self.main_window)
            self.date_title = tkinter.Label(self.date_frame,text='Date:')
            self.date_year_var = tkinter.StringVar()
            self.date_year_var.set(self.year)
            self.date_month_var = tkinter.StringVar()
            self.date_month_var.set(self.month_name)
            self.date_day_var = tkinter.StringVar()
            self.date_day_var.set(self.day)
            self.date_year_label = tkinter.Label(self.date_frame,textvariable=self.date_year_var)
            self.date_month_label = tkinter.Label(self.date_frame,textvariable=self.date_month_var)
            self.date_day_label = tkinter.Label(self.date_frame,textvariable=self.date_day_var)
            self.date_year_entry_start = tkinter.Entry(self.date_frame,text='After:',textvariable=self.year_start_var,width='10')
            self.date_year_entry_end = tkinter.Entry(self.date_frame,text='Before:',textvariable=self.year_end_var,width='10')
            self.year_error = tkinter.Label(self.date_frame,textvariable=self.year_error_text)
            self.date_button = tkinter.Button(self.date_frame,text='Change Date',command=self.change_date)
            
            # Packing date module and frame
            self.date_title.pack(side='left')
            self.date_month_label.pack(side='left')
            self.date_day_label.pack(side='left')
            self.date_year_label.pack(side='left')
            self.date_frame.pack(fill="x", padx="3", pady="3")
            self.year_error.pack(side='right')
            self.date_button.pack(side='right')
            self.date_year_entry_end.pack(side='right')
            self.date_year_entry_start.pack(side='right')

            # Creating a panel for randomize all button and text
            self.bottom_frame = tkinter.Frame(self.main_window)
            self.randomize_all_button = tkinter.Button(self.bottom_frame,text='Randomize All',command=self.randomize_all)
            self.exported_text = tkinter.StringVar()
            self.attributes = Attributes()
            self.exported_text.set(
                   
                  str(self.attributes.first_name)+' '+str(self.attributes.middle_name)+' '+str(self.attributes.last_name)+' is a '+
                  str(self.attributes.age)+' year old '+str(self.attributes.age_range)+' '+str(self.attributes.ethnicity)+" "+str(self.attributes.gender)
                  +' with '+str(self.attributes.hair)+' hair and '+str(self.attributes.eye_color)+' eyes. '
                  +str(self.attributes.first_name)+' is '+str(format(self.attributes.height/12,',.2f'))+' feet tall '
                  +'and weighs '+str(format(self.attributes.weight,',.2f'))+' pounds.'
                  +' On '+str(self.date_month_var.get())+'/'+str(self.date_day_var.get())+'/'+str(self.date_year_var.get())+' '+
                  str(self.attributes.first_name)+' lives in '+str(self.city_var.get())+' ,'+str(self.state_var.get())+' '+str(self.country_var.get())
            )
            self.exported_text_button = tkinter.Button(self.bottom_frame,text='Copy to Clip Board',command=self.copy_to_clipboard)
            
            self.randomize_all_button.pack(side='left')
            self.exported_text_button.pack()
            
            
            self.bottom_frame.pack()
            self.randomize_all_button.pack()

            
            
      # Defineing Functions For Buttons

      def change_name(self):
            self.attributes = Attributes()
            self.fn_text.set(self.attributes.first_name)
            self.mn_text.set(self.attributes.middle_name)
            self.ln_text.set(self.attributes.last_name)
      def change_fname(self):
            
            self.attributes = Attributes()
            self.fn_text.set(self.attributes.first_name)
      def change_mname(self):
            
            self.attributes = Attributes()
            self.mn_text.set(self.attributes.middle_name)
      def change_lname(self):
            
            self.attributes = Attributes()
            self.ln_text.set(self.attributes.last_name)
      def change_ethnicity(self):
            
            self.attributes = Attributes()
            self.eth_var.set(self.attributes.ethnicity)
      def change_age(self):
            
            self.attributes = Attributes()
            self.age_var.set(self.attributes.age)
            self.age_name_var.set(self.attributes.age_range)
      def change_hair(self):
            
            self.attributes = Attributes()
            self.hair_text.set(self.attributes.hair)
      def change_eye_color(self):
            
            self.attributes = Attributes()
            self.eye_text.set(self.attributes.eye_color)
      def change_gender(self):
            
            self.attributes = Attributes()
            self.gender_var.set(self.attributes.gender)
      def change_country(self):
            
            self.attributes = Attributes()
            self.country_var.set(self.attributes.country_name)
            self.state_var.set(self.attributes.state_name)
            self.city_var.set(self.attributes.town_name)
            # Setting current country and state variables for the
            # change state and city functions just like the beginning.
            self.v_country = self.attributes.country
            self.v_state = self.attributes.state
            
            
      def change_state(self):
            
            self.attributes = Attributes()
            self.state_var.set(self.attributes.get_new_state(self.v_country))
            self.v_state = self.attributes.state
            self.city_var.set(self.attributes.get_new_town(self.v_country,self.v_state))
            
      def change_city(self):
            
            self.attributes = Attributes()
            self.city_var.set(self.attributes.get_new_town(self.v_country,self.v_state))
      def change_date(self):
            if int(self.year_start_var.get()) > int(self.year_end_var.get()):
                  self.year_start_var.set('1000')
                  self.year_end_var.set('2020')
      
            self.year = random.randint(int(self.year_start_var.get()),int(self.year_end_var.get()))
            self.month_val = random.randint(1,len(self.month_days)-1)
            self.day = random.randint(1,self.month_days[self.month_val][1])
            self.month_name = self.month_days[self.month_val][0]
            

            self.date_month_var.set(self.month_name)
            self.date_day_var.set(self.day)
            self.date_year_var.set(self.year)
      def randomize_all(self):
            self.attributes = Attributes()
            self.fn_text.set(self.attributes.first_name)
            self.mn_text.set(self.attributes.middle_name)
            self.ln_text.set(self.attributes.last_name)

            self.eth_var.set(self.attributes.ethnicity)

            self.gender_var.set(self.attributes.gender)

            self.eye_text.set(self.attributes.eye_color)

            self.hair_text.set(self.attributes.hair)

            self.age_var.set(self.attributes.age)
            self.age_name_var.set(self.attributes.age_range)
            
            self.country_var.set(self.attributes.country_name)
            self.state_var.set(self.attributes.state_name)
            self.city_var.set(self.attributes.town_name)
            # Setting current country and state variables for the
            # change state and city functions just like the beginning.
            self.v_country = self.attributes.country
            self.v_state = self.attributes.state

            if int(self.year_start_var.get()) > int(self.year_end_var.get()):
                  self.year_start_var.set('1000')
                  self.year_end_var.set('2020')
      
            self.year = random.randint(int(self.year_start_var.get()),int(self.year_end_var.get()))
            self.month_val = random.randint(1,len(self.month_days)-1)
            self.day = random.randint(1,self.month_days[self.month_val][1])
            self.month_name = self.month_days[self.month_val][0]
            

            self.date_month_var.set(self.month_name)
            self.date_day_var.set(self.day)
            self.date_year_var.set(self.year)
      def copy_to_clipboard(self):
            
            self.main_window.clipboard_clear
            self.main_window.clipboard_append(str(self.exported_text.get()))
      
            
      
            
            
            
            tkinter.mainloop()
            


mygui = MyGui()
